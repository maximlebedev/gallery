package com.example.gallery;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

public class FullscreenImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fullscreen_layout);

        ImageView imageView = findViewById(R.id.image);
        String path = getIntent().getStringExtra("PATH");

        Glide.with(this)
                .load(path)
                .into(imageView);

    }
}
